# Файлы для интеграции aQsi Online


| File | Rus  | Eng  |
| ---- | ---- | ---- |
|  [SignatureGenerator](./SignatureGenerator)    |  Программа для генерации подписи  X-Signature при отправке запросов к API aQsi Online через POSTMAN (в качестве тестов)  |  X-Signature generator for POSTMAN API tests   |
| [aQsi.Online_API_Russian.docx](./aQsi.Online_API_Russian.docx) |  Описание API на русском языке  |     API Russuan  |
|  [aQsi.Online_API_English.docx](./aQsi.Online_API_English.docx)    |   Описание API на английском языке   |   API English   |
|  [Nebula.KeysGenerator-1.2.0.0.zip](./Nebula.KeysGenerator-1.2.0.0.zip)    |   Генератор ключей для работы с продуктовой средой aQsi Online   | Key generator for production environment |

